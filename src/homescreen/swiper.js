import React, { Component } from 'react';
import SwipeableParallaxCarousel from 'react-native-swipeable-parallax-carousel';

// const datacarousel = [
//     {
//         "id": 339964,
//         "imagePath": "https://temanberbagi.s3.amazonaws.com/promo/a2bdb8de-ea82-4f8e-bc5d-cffc69bf776f.png",
//     },
//     {
//         "id": 315635,
//         "imagePath": "https://image.tmdb.org/t/p/w780/fn4n6uOYcB6Uh89nbNPoU2w80RV.jpg",
//     },
//     {
//         "id": 339403,
//         "title": "Baby Driver",
//         "subtitle": "More than just a trend",
//         "imagePath": "https://image.tmdb.org/t/p/w780/xWPXlLKSLGUNYzPqxDyhfij7bBi.jpg",
//     },
// ];

export default class Swiper extends Component {

    constructor() {
        super();
        this.state = {
            datacarousel: [],
            datacarousel1: [{ imagePath: null }]
        };
    }

    componentDidMount() {
        fetch("http://ec2-54-255-226-10.ap-southeast-1.compute.amazonaws.com:9009/api/banner", {
            method: "GET",
            headers: {
                'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
            }
        })
            .then((response) => response.json())
            .then((data) => {

                this.setState({
                    datacarousel: data.data
                });

            })
            .catch((error) => {
                console.log(error);
            })
    }

    render() {
        let carouseldata = [];

        this.state.datacarousel.map((item, idx) => {
            let data = {
                id: item._id,
                imagePath: item.imagesBanner
            }
            carouseldata.push(data)
        })
        this.state.datacarousel = carouseldata;
        // console.log(carouseldata)
        return (
            <SwipeableParallaxCarousel
                data={carouseldata}
                navigation={true}
                navigationType={'dots'}
                height={150}
                delay={2000}
            />
        );
    }
}
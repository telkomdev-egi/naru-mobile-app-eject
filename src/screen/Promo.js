import React from "react";
import { Image, AsyncStorage, TouchableOpacity } from "react-native";
import Expo from "expo";
import {
  Container,
  Header,
  Content,
  Left,
  Right,
  Body,
  Title,
  Button,
  Icon,
  Item,
  Input,
  Card,
  CardItem,
  Footer,
  FooterTab,
  Text
} from "native-base";
import { AppHeader, AppFooter } from "../app-nav/index"
import firebasePromo from "../firebase/firebasePromo"

export default class Promo extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      promo: [],
      isReady: false
    }
  }
  componentDidMount() {
    //no18 id detail masih salah
    AsyncStorage.getItem("bumnId", (error, result) => {
      fetch("http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/listpromos/", {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      })
        .then(response => response.json())
        .then((data) => {
          if (data.length > 0) {
            this.setState({ promo: data });
          }
          this.setState({ isReady: true });
        })
    })
  }
  render() {
    if (!this.state.isReady) {
      
      return <Expo.AppLoading />;
      <firebasePromo/>
    }
    return (
      
      <Container>
        <AppHeader navigation={this.props.navigation} title="Promo" />
        <Content>
          <Card>
            <CardItem cardBody style={{ height: 160, marginLeft: 10, marginRight: 10, justifyContent: 'space-between', flexDirection: "row" }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a464516f2b59340420692d3' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/1.Inidhome-freechannelnatal-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a464593e93be3467484d320' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/2.Indihome-paketnatal-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
            </CardItem>
          </Card>
          <Card>
            <CardItem cardBody style={{ height: 160, marginLeft: 10, marginRight: 10, justifyContent: 'space-between', flexDirection: "row" }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a464630e93be3467484d321' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/3.Indihomepialapresiden-min.jpeg")} resizeMode='contain' />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a4646715289ab466b476db4' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/4.indihome-speedondemand-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
            </CardItem>
          </Card>
          <Card>
            <CardItem cardBody style={{ height: 160, marginLeft: 10, marginRight: 10, justifyContent: 'space-between', flexDirection: "row" }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a4646a95289ab466b476db5' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/5.Inidhome-minipack-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a4646df5289ab466b476db6' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/6.Indihome-HOOQ-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
            </CardItem>
          </Card>
          <Card>
            <CardItem cardBody style={{ height: 160, marginLeft: 10, marginRight: 10, justifyContent: 'space-between', flexDirection: "row" }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a4647145289ab466b476db7' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/7.Indihome-Iflix-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a46474f5289ab466b476db8' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/8.Indihome-wifi.id-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
            </CardItem>
          </Card>
          <Card>
            <CardItem cardBody style={{ height: 160, marginLeft: 10, marginRight: 10, justifyContent: 'space-between', flexDirection: "row" }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a46479e5289ab466b476db9' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/9.Indihomemovin-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a4647d35289ab466b476dba' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/10.Inidhome-globalcall-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
            </CardItem>
          </Card>
          <Card>
            <CardItem cardBody style={{ height: 160, marginLeft: 10, marginRight: 10, justifyContent: 'space-between', flexDirection: "row" }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a4648625289ab466b476dbb' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/11.Inidhome-catchplay-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a46489aa63b133fcf226d28' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/12.Telkomsel-dapatpulsaloop-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
            </CardItem>
          </Card>
          <Card>
            <CardItem cardBody style={{ height: 160, marginLeft: 10, marginRight: 10, justifyContent: 'space-between', flexDirection: "row" }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a4648dba63b133fcf226d29' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/13.BLanja-promobca-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a46492db82a954b758c7f50' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/14.Telkomsel-cashback20-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
            </CardItem>
          </Card>
          <Card>
            <CardItem cardBody style={{ height: 160, marginLeft: 10, marginRight: 10, justifyContent: 'space-between', flexDirection: "row" }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a46497bb82a954b758c7f51' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/15.telkomsel-startbucks-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a46497bb82a954b758c7f51' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/16.Telkomsel-Vaganza-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
            </CardItem>
          </Card>
          <Card>
            <CardItem cardBody style={{ height: 160, marginLeft: 10, marginRight: 10, justifyContent: 'space-between', flexDirection: "row" }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a46489aa63b133fcf226d28' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/17.Telkomsel-NSPAyatayatcintatelkomsel-min.jpg")} resizeMode='contain' />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a44faed7aaebc45107b959e' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/18.Blanja-cucigudang.jpeg")} resizeMode='contain' />
              </TouchableOpacity>
            </CardItem>
          </Card>
          <Card>
            <CardItem cardBody style={{ height: 160, marginLeft: 10, marginRight: 10, justifyContent: 'space-between', flexDirection: "row" }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a46492db82a954b758c7f50' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/19.Uwarnet.jpg")} resizeMode='contain' />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("PromoDetail", { idBerita: '5a463adec558cf43b286ef98' })}>
                <Image style={{ width: 160 }} source={require("../../img/asset/promo/20.Tmoney.jpeg")} resizeMode='contain' />
              </TouchableOpacity>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }

};


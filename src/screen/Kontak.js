import React from "react";
import { Image, StyleSheet, TouchableOpacity } from "react-native";
import {
    Text,
    Container,
    Content,
    Button,
    Thumbnail,
    Grid,
    Row,
    Col,
    View,
    Card,
    CardItem
} from "native-base";
import { AppHeader, AppFooter } from "../app-nav/index";
import call from 'react-native-phone-call';
import Modal from 'react-native-modal';

export default class Kontak extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isModalVisible: false,
        }
    }
    render() {
        return (
            <Container style={{ backgroundColor: "#FFF" }}>
                <AppHeader isMenu navigation={this.props.navigation} title="Telepon Penting" />
                <Content>
                    <Text style={styles.titleText}>
                    </Text>
                    <Text style={styles.titleText}>
                        Informasi Telepon Penting
                </Text>
                    <Grid>
                        <Row>
                            <Col style={{ height: 150, justifyContent: 'flex-end' }}>
                                <Button style={{ height: 100, width: 153, alignSelf: 'center' }} transparent onPress={this.callBencanaAlam}>
                                    <Image style={{ height: 100, width: 153 }} source={require("../../img/asset/bencana_alam.png")} />
                                </Button>
                            </Col>
                            <Col style={{ height: 150, justifyContent: 'flex-end' }}>
                                <Button style={{ height: 100, width: 153, alignSelf: 'center' }} transparent onPress={this.callPolisi}>
                                    <Image style={{ height: 100, width: 153 }} source={require("../../img/asset/kantor_polisi.png")} />
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{ height: 150, justifyContent: 'center' }}>
                                <Button style={{ height: 100, width: 153, alignSelf: 'center' }} transparent onPress={this.callPemadamKebakaran}>
                                    <Image style={{ height: 100, width: 153 }} source={require("../../img/asset/pemadam_kebakaran.png")} />
                                </Button>
                            </Col>
                            <Col style={{ height: 150, justifyContent: 'center' }}>
                                <Button style={{ height: 100, width: 153, alignSelf: 'center' }} transparent onPress={this.callAmbulan}>
                                    <Image style={{ height: 100, width: 153 }} source={require("../../img/asset/ambulan.png")} />
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{ height: 150, justifyContent: 'center' }}>
                                <Button style={{ height: 100, width: 153, alignSelf: 'center' }} transparent onPress={this.callTelkom}>
                                    <Image style={{ height: 100, width: 153 }} source={require("../../img/asset/no_telkom.png")} />
                                </Button>
                            </Col>
                            <Col style={{ height: 150, justifyContent: 'center' }}>
                                <Button style={{ height: 100, width: 153, alignSelf: 'center' }} transparent onPress={this._showModal}>
                                    <Image style={{ height: 100, width: 153 }} source={require("../../img/asset/no_telkomsel.png")} />
                                </Button>
                            </Col>
                        </Row>
                    </Grid>

                    <Modal isVisible={this.state.isModalVisible} style={{ flex: 1, backgroundColor: "white", marginTop: "50%", marginBottom: "50%", borderRadius: 5 }}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <TouchableOpacity onPress={this._hideModal}>
                            <Image style={{ height: 30, width: 30 }} source={require("../../img/asset/ic_close.png")} />
                        </TouchableOpacity>
                        </View>
                        <View style={styles.contentContainer}>
                            <Text style={styles.baseText}>Anda menghubungi call center</Text>
                            <Text style={styles.baseText}>telkomsel menggunakan ?</Text>
                        </View>
                        <View style={{ marginTop: "10%", flex: 1, justifyContent: 'space-between', flexDirection: "row" }}>
                            <TouchableOpacity onPress={this.callTelkomsel} style={{ paddingLeft: "10%", alignItems: 'center' }}>
                                <Text style={styles.baseText}>Nomor</Text>
                                <Text style={styles.baseText}>Telkomsel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.callnonTelkomsel} style={{ paddingRight: "10%", alignItems: 'center' }}>
                                <Text style={styles.baseText}>Nomor non</Text>
                                <Text style={styles.baseText}>Telkomsel</Text>
                            </TouchableOpacity>
                        </View>
                    </Modal>
                </Content>
                {/*<AppFooter navigation={this.props.navigation} />*/}
            </Container>
        );
    }

    go(param) {
        if (param == "kontak") this.props.navigation.navigate("Kontak");
    }
    callPemadamKebakaran = () => {
        <Button onPress={call(pemadamKebakaran).catch(console.error)}></Button>
    }

    callAmbulan = () => {
        <Button onPress={call(ambulan).catch(console.error)} ></Button>
    }
    callPolisi = () => {
        <Button onPress={call(polisi).catch(console.error)}></Button>
    }
    callBencanaAlam = () => {
        <Button onPress={call(bencanaAlam).catch(console.error)} ></Button>
    }
    callTelkom = () => {
        <Button onPress={call(telkom).catch(console.error)} ></Button>
    }
    callTelkomsel = () => {
        <Button onPress={call(telkomsel).catch(console.error)} ></Button>
        this.setState({ isModalVisible: false })
    }

    callnonTelkomsel = () => {
        <Button onPress={call(nontelkomsel).catch(console.error)} ></Button>
        this.setState({ isModalVisible: false })
    }

    _showModal = () => {
        this.setState({ isModalVisible: true })
    }
    _hideModal = () => {
    this.setState({ isModalVisible: false })
  }
};

const styles = StyleSheet.create({
    titleText: {
        fontFamily: 'Ubuntu_Regular',
        fontSize: 20,
        fontWeight: 'bold',
        marginLeft: "5%"
    },
    baseText: {
        fontFamily: 'Ubuntu_Light',

    },
    container: {
        flex: 0.5,
        alignItems: 'center',
    },
    contentContainer: {
        borderWidth: 0,
        borderColor: '#FFF',
        flex: 1,
        alignItems: 'center',
        marginTop: "10%"
    },
    footer: {
        backgroundColor: "#fff"
    },
}
)

const pemadamKebakaran = {
    number: '118', // String value with the number to call
    prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}


const ambulan = {
    number: '113', // String value with the number to call
    prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}
const polisi = {
    number: '110', // String value with the number to call
    prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}


const bencanaAlam = {
    number: '129', // String value with the number to call
    prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}

const telkom = {
    number: '147', // String value with the number to call
    prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}

const telkomsel = {
    number: '188', // String value with the number to call
    prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}

const nontelkomsel = {
    number: '08071811811', // String value with the number to call
    prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}



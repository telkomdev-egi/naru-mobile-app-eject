import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import * as firebase from 'firebase';
import timestamp from 'time-stamp';

export default class firebaseNews extends React.Component {
  render() {
    fireData('news','12345');
    return (
      <View style={styles.container}>
        <Text>Firebase.IO</Text>
      </View>
    );
  }
}

const fireData = (type,objectId) => {
  const sendData = (type,objectId,identifierId) => {
    firebase.database().ref(type + '/').set({
      key: "49282a17-8911-a895-fc1d-0ea67e277d09",
      value: "12345"
    });
  }
  const config = {
    apiKey: "AIzaSyALd8vvE2-qtPKnUzHuX2XkrmUuLH_QyzQ",
    authDomain: "naru-app.firebaseapp.com",
    databaseURL: "https://naru-app.firebaseio.com",
    projectId: "naru-app",
    storageBucket: "naru-app.appspot.com",
    messagingSenderId: "1087431159499"
  };
  firebase.initializeApp(config);
  identifierId = timestamp('YYYY/MM/DD HH:mm:ss');
  sendData(type,objectId,identifierId);
}

const guid = () => {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
